__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

import random

from django.http import Http404
from collections import OrderedDict
from django.core.urlresolvers import reverse
from pure_pagination.mixins import PaginationMixin
from django.utils.translation import ugettext_lazy as _
from django.views.generic import TemplateView, ListView, DetailView

from .models import ReadyObject, BuildingSet, Furniture,\
    Material, SpecialEquipment

from django.conf import settings

OFFER_TYPES = OrderedDict([
    (settings.MATERIAL, {
        'model': Material,
        'name': settings.MATERIAL,
        'display_name': _('Material'),
        'display_name_plural': _('Materials'),
        'types': Material.TYPES,
    }),
    (settings.BUILDING_SET, {
        'model': BuildingSet,
        'name': settings.BUILDING_SET,
        'display_name': _('Building Set'),
        'display_name_plural': _('Building Sets'),
    }),
    (settings.FURNITURE, {
        'model': Furniture,
        'name': settings.FURNITURE,
        'display_name': _('Furniture for Home and Garden'),
        'display_name_plural': _('Furniture for Home and Garden'),
    }),
    (settings.SPECIAL_EQUIPMENT, {
        'model': SpecialEquipment,
        'name': settings.SPECIAL_EQUIPMENT,
        'display_name': _('Special Equipment'),
        'display_name_plural': _('Special Equipment'),
    }),
    (settings.READY, {
        'model': ReadyObject,
        'name': settings.READY,
        'display_name': _('Ready Object'),
        'display_name_plural': _('Ready Objects'),
        'types': ReadyObject.TYPES,
    }),
])

OFFER_MODELS = [v['model'] for i, v in OFFER_TYPES.items()]


class Main(TemplateView):
    template_name = 'main/main.html'

    def get_context_data(self, **kwargs):
        context = super(Main, self).get_context_data(**kwargs)
        obj_list = []
        break_flag = 0
        while len(obj_list) < 6 and break_flag < 15:
            model = random.choice(OFFER_MODELS)
            id_list = model.objects.values_list('pk', flat=True)
            if id_list:
                obj_list.append(model.objects.get(pk=random.choice(id_list)))
            break_flag += 1
        context['others'] = obj_list
        return context


class Contact(TemplateView):
    template_name = 'main/contact.html'

    def get_context_data(self, **kwargs):
        context = super(Contact, self).get_context_data(**kwargs)
        context['breadcrumb_headers'] = [
            {'name': _('Contact'), 'url': '#'},
        ]
        return context


class OfferTypeMixin(object):
    def get(self, request, *args, **kwargs):
        self.offer_type = kwargs.get('offer_type')
        if self.offer_type in OFFER_TYPES.keys():
            self.offer_type = OFFER_TYPES[self.offer_type]
            return super(OfferTypeMixin, self).get(request, *args, **kwargs)
        else:
            raise Http404

    def get_queryset(self):
        self.model = self.offer_type['model']
        result = super(OfferTypeMixin, self).get_queryset()
        return result


class ObjectTypeMixin(object):

    def get_queryset(self):
        result = super(ObjectTypeMixin, self).get_queryset()
        self.object_type = self.kwargs.get('object_type')
        if self.object_type != settings.DEFAULT_OBJECT_TYPE and \
            self.object_type not in [i[0] for i in self.model.TYPES]:
            raise Http404
        if hasattr(self.model, 'TYPES') \
                and self.object_type != settings.DEFAULT_OBJECT_TYPE:
            result = result.filter(object_type=self.object_type)
        return result


class ObjectList(PaginationMixin, ObjectTypeMixin, OfferTypeMixin, ListView):
    model = ReadyObject
    paginate_by = 6
    template_name = 'main/object_list.html'

    def get_context_data(self, **kwargs):
        context = super(ObjectList, self).get_context_data(**kwargs)
        context['breadcrumb_headers'] = [
            {'name': self.offer_type['display_name_plural'], 'url': '#'},
        ]
        if self.object_type != settings.DEFAULT_OBJECT_TYPE:
            context['breadcrumb_headers'][0]['url'] = \
                reverse('object_list',
                        kwargs={'offer_type': self.offer_type['name'],
                                'object_type': settings.DEFAULT_OBJECT_TYPE})
            li_t = filter(lambda x: x[0] == self.object_type, self.model.TYPES)
            context['breadcrumb_headers'].append(
                {'name': li_t[0][1] if li_t else settings.DEFAULT_OBJECT_TYPE,
                 'url': '#'},
            )
        return context


class ObjectDetail(OfferTypeMixin, ObjectTypeMixin, DetailView):
    model = ReadyObject
    context_object_name = 'object'
    template_name = 'main/object_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ObjectDetail, self).get_context_data(**kwargs)
        id_list = self.model.objects.exclude(pk=self.object.pk)\
                      .values_list('pk', flat=True)
        amount = 3
        if len(id_list) < amount:
            amount = len(id_list)
        obj_list = self.model.objects.exclude(pk=self.object.pk)\
                       .filter(pk__in=random.sample(id_list, amount))
        context['others'] = obj_list
        obj_type_url = reverse('object_list',
                            kwargs={'offer_type': self.offer_type['name'],
                                    'object_type': self.object_type})
        obj_type_url_all = reverse('object_list',
                           kwargs={'offer_type': self.offer_type['name'],
                                   'object_type': settings.DEFAULT_OBJECT_TYPE})

        context['breadcrumb_headers'] = [
            {'name': self.offer_type['display_name_plural'],
             'url': obj_type_url_all},
        ]
        if self.object_type != settings.DEFAULT_OBJECT_TYPE:
            context['breadcrumb_headers'].append(
                {'name': self.object.get_object_type_display(),
                 'url': obj_type_url},
            )
        context['breadcrumb_headers'].append(
            {'name': self.object.name, 'url': '#'},
        )
        return context